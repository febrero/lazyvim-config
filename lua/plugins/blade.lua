return {
  {
    "jwalton512/vim-blade", -- Soporte para archivos Blade
    ft = "blade", -- Cargar solo para archivos .blade.php
    config = function()
      -- Configuración opcional aquí
    end,
  },
}
