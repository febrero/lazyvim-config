return {
  {
    "yetone/avante.nvim",
    event = "VeryLazy",
    lazy = false,
    version = false, -- set this if you want to always pull the latest change
    opts = {
      provider = "openai",
      auto_suggestions_provider = "openai", -- Since auto-suggestions are a high-frequency operation and therefore expensive, it is recommended to specify an inexpensive provider or even a free provider: copilot
      openai = {
        endpoint = "https://api.deepseek.com/v1",
        model = "deepseek-chat",
        timeout = 30000, -- Timeout in milliseconds
        temperature = 0,
        max_tokens = 4096,
        -- optional
        api_key_name = "javier", -- default OPENAI_API_KEY if not set
      },
      prompts = {
        generate_commit = {
          description = "Generate conventional commit message",
          template = function()
            -- Obtener diff del repositorio
            local diff = vim.fn.system("git diff --staged")
            -- Plantilla para el mensaje de commit
            return [[
              Genera un mensaje de commit convencional en español usando el siguiente diff.
              Considera:
              1. Tipo de commit (feat, fix, chore, docs, style, refactor, test)
              2. Breve descripción (50 caracteres máximo)
              3. Cuerpo detallado (opcional)
              4. Footer con referencia a issues (opcional)

              Ejemplo de formato:
              feat: agregar sistema de autenticación
              
              - Implementado login con JWT
              - Añadido middleware de autenticación
              - Configurado almacenamiento seguro de tokens
              
              Ref: #123

              Diff:
              ]] .. diff
          end,
        },
      },
    },
    -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
    build = "make",
    -- build = "powershell -ExecutionPolicy Bypass -File Build.ps1 -BuildFromSource false" -- for windows
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "stevearc/dressing.nvim",
      "nvim-lua/plenary.nvim",
      "MunifTanjim/nui.nvim",
      --- The below dependencies are optional,
      "nvim-tree/nvim-web-devicons", -- or echasnovski/mini.icons
      "zbirenbaum/copilot.lua", -- for providers='copilot'
      {
        -- support for image pasting
        "HakonHarnes/img-clip.nvim",
        event = "VeryLazy",
        opts = {
          -- recommended settings
          default = {
            embed_image_as_base64 = false,
            prompt_for_file_name = false,
            drag_and_drop = {
              insert_mode = true,
            },
            -- required for Windows users
            use_absolute_path = true,
          },
        },
      },
      {
        -- Make sure to set this up properly if you have lazy=true
        "MeanderingProgrammer/render-markdown.nvim",
        opts = {
          file_types = { "markdown", "Avante" },
        },
        ft = { "markdown", "Avante" },
      },
    },
  },
}
