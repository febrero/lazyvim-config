return {
  -- add gruvbox
  { "ellisonleao/gruvbox.nvim" },
  -- rebelot/kanagawa.nvim
  { "rebelot/kanagawa.nvim" },
  { "nyoom-engineering/nyoom.nvim" },
  { "catppuccin/nvim", name = "catppuccin", lazy = false, priority = 1000 },
  { "bluz71/vim-nightfly-colors", name = "nightfly", lazy = false, priority = 1000 },
  {
    "scottmckendry/cyberdream.nvim",
    lazy = false,
    priority = 1000,
    opts = function()
      return {
        transparent = true,
      }
    end,
  },
  { "bluz71/vim-moonfly-colors", name = "moonfly", lazy = false, priority = 1000 },
  {
    "folke/tokyonight.nvim",
    lazy = false,
    opts = function()
      return { transparent = true }
    end,
  },
  { "rose-pine/neovim", name = "rose-pine" },
  { "diegoulloao/neofusion.nvim", priority = 1000, config = true, opts = ... },
  { "xiyaowong/transparent.nvim" },
  { "samharju/serene.nvim" },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "rose-pine",
    },
  },
}
