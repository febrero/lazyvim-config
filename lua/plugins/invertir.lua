return {
  {
    "nguyenvukhang/nvim-toggler",
    config = function()
      require("nvim-toggler").setup({
        inverses = {
          ["true"] = "false",
          ["0"] = "1",
          ["yes"] = "no",
          ["on"] = "off",
          ["enable"] = "disable",
          ["up"] = "down",
          ["left"] = "right",
        },
      })

      -- Definir keymap personalizado con un icono
      local icon = "⇄"
      vim.keymap.set("n", "<leader>t", function()
        require("nvim-toggler").toggle()
      end, { noremap = true, silent = true, desc = icon .. " Toggle word under cursor" })
    end,
  },
}
